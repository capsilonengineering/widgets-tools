var widgets = [
    {
        name: "Conditions",
        id: 'conditions',
        path: "/conditions-widget/dist/common/config/app.json"
    },
    {
        name: "Doc Auditor",
        id: 'doc_auditor',
        path: "/docexplorer/dist/common/config/app.json"
    },
    {
        name: "Doc Reader",
        id: 'doc_reader',
        path: "/docreader/javascripts/src/config/app.json"
    },
    {
        name: "Submission widget",
        id: 'submission_widget',
        path: "/htmlsubmissionapp/dist/common/config/app.json"
    },
    {
        name: "Messages",
        id: 'messages',
        path: "/thread-app/dist/common/config/app.json"
    },
    {
        name: "Filed Docs App",
        id: 'fileddocs',
        path: "/fileddocs/app.json"
    },
    {
        name: "Received Mail",
        id: 'receivedmailapp',
        path: "/received-mail-app/app.json"
    }
];

var siteField = document.getElementById("site");
var table = document.getElementById("grid").getElementsByTagName('tbody')[0];
var appName = document.getElementById("app-name");
var appSecret = document.getElementById("app-secret");
var output = document.getElementById("output");
var counter = document.getElementById("counter");

var getVersions = function() {
    if (siteFieldIsEmpty()) {
        return;
    }
    $(table).empty();
    widgets.forEach(function(w) {
        w.appJson = undefined;
    });
    getValidatedWidgetsLength();
    widgets.forEach(function(el, index) {
        checkAppJSONVersion(getSiteFieldValue(), el, appendRow);
    });
};

var checkAppJSONVersion = function(site, widget, callback) {
    $.ajax({
        type: "GET",
        url: "https://" + site + widget.path,
        success: function(res) {
            widgets.filter(function(w) {
                return w.name === widget.name;
            })[0].appJson = res;
            callback(widget, res);
        },
        error: function(err) {
            widgets.filter(function(w) {
                return w.name === widget.name;
            })[0].appJson = null;
            callback(widget, false, "https://" + site + widget.path);
        }
    });
};

var validateManually = function () {
    if (siteFieldIsEmpty() || appCredentialsFieldsAreEmpty()) {
        return;
    }
    validateApplication(false, appName.value, appSecret.value)
};

var validateApplication = function(name, appName, appSecret) {
    var widget = widgets.filter(function(w) {
        return w.id === name;
    });
    var data = {
        applicationName: appName ? appName.trim() : widget[0].appJson.name,
        applicationSecret: appSecret ? appSecret.trim() : widget[0].appJson.secret,
        siteAddress: getSiteFieldValue()
    };

    $.ajax({
        type: "POST",
        url: "https://" + getSiteFieldValue() + "/HUB/services/rest/ApplicationService/validateApplicationAndSite",
        data: data,
        success: function(res) {
            printOutput(JSON.stringify({
                "widget name": widget[0] ? widget[0].name : appName.trim(),
                "registered": "yes",
                "urls": res.validateApplicationAndSiteResult.urls
            }));
        },
        error: function(err) {
            printOutput(JSON.stringify({
                "widget name": widget[0] ? widget[0].name : appName.trim(),
                "registered": "no",
                "error": JSON.parse(err.responseText)
            }));
        }
    });
};

var appendRow = function(widget, res, path) {
    var el;
    var row = table.insertRow();
    if (!res) {
        el = "<td>" + widget.name + "</td><td></td><td><a href='" + path + "' target='_blank'>Check Manually</a></td><td></td>";
    } else {
        var button =
            el = "<td>" + widget.name + "</td><td>" + widget.appJson.name + "</td><td>" + res.version + "</td><td id=\"" + widget.id + "\"><button onclick=\"validateApplication('" + widget.id + "')\">Validate</button></td>";
    }
    row.innerHTML = el;

    getValidatedWidgetsLength();
};

var printOutput = function (data) {
    output.value += '' + data + '\n\n';
};

var siteFieldIsEmpty = function () {
    if (!getSiteFieldValue().length) {
        alert("Enter Site Address");
        return true;
    }
};

var appCredentialsFieldsAreEmpty = function () {
    if (!getAppName().length || !getAppSecret().length) {
        alert("Enter Application Name and Secret");
        return true;
    }
};

var getValidatedWidgetsLength = function () {
    counter.innerText = "Information received for " + widgets.filter(function (el) {
        return typeof el.appJson !== "undefined";
    }).length + ' of ' + widgets.length + " widgets";
};

var getSiteFieldValue = function() {
    return siteField.value.trim();
};

var getAppName = function() {
    return appName.value.trim();
};

var getAppSecret = function() {
    return appSecret.value.trim();
};

getValidatedWidgetsLength();
